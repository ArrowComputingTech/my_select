#!/home/hz/.rbenv/shims/ruby
require './eech.rb'

module Enumerable
  def my_select
    retr = []
    self.eech do |x|
      if yield(x)
        retr << x
      end
    end
    return retr
  end
end

arr = [2,2,7,4,5,6].my_select { |n| n.even? }
puts arr.join(',')
